﻿using CustomerApi.Entities;
using CustomerApi.Repositories;
using CustomerApi.Utils;

namespace CustomerApi.Services
{
  public class CustomerService : ICustomerService
  {
    private readonly ICustomerRepository _customerRepository;
    public CustomerService(ICustomerRepository customerRepository)
    {
      _customerRepository = customerRepository;
    }

    void ICustomerService.Add(Customer customer)
    {
      var isValidEmail = Validator.isValidEmail(customer.Email);

      if (!isValidEmail)
      {
        throw new BadHttpRequestException("Invalid email format");
      }

      _customerRepository.Add(customer);
    }

    IEnumerable<Customer> ICustomerService.GetAll(int offset, int limit)
    {
      return _customerRepository.GetAll(offset, limit);
    }

    void ICustomerService.Remove(int customerId)
    {
      _customerRepository.Remove(customerId);
    }

    Customer ICustomerService.SearchById(int customerId)
    {
      var customer = _customerRepository.SearchById(customerId);

      if (customer == null)
      {
        throw new BadHttpRequestException("Customer not found", 404);
      }

      return customer;
    }

    void ICustomerService.Update(Customer customer)
    {
      if (customer.Id <= 0)
      {
        throw new BadHttpRequestException("Customer id should be more than zero", 400);
      }
      _customerRepository.Update(customer);
    }
  }
}
