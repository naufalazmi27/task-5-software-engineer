﻿using CustomerApi.Entities;

namespace CustomerApi.Services
{
    public interface ICustomerService
    {
        void Add(Customer customer);
        IEnumerable<Customer> GetAll(int offset, int limit);
        Customer SearchById(int customerId);
        void Update(Customer customer);
        void Remove(int customerId);
    }
}
