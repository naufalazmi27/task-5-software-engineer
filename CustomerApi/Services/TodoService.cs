﻿using CustomerApi.Entities;

namespace CustomerApi.Services
{
  public class TodoService : ITodoService
  {

    private readonly HttpClient _httpClient;
    private readonly string baseUrl = "https://jsonplaceholder.typicode.com/";

    public TodoService(HttpClient httpClient)
    {
      _httpClient = httpClient;

      _httpClient.BaseAddress = new Uri(baseUrl);
      _httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
    }
    public async Task<List<Todo>> GetAll()
    {
      var response = await _httpClient.GetFromJsonAsync<IEnumerable<Todo>>("todos");
      return response == null ? new List<Todo>() : response.ToList();
    }

    public Todo GetById(int id)
    {
      var response = _httpClient.GetFromJsonAsync<Todo>($"todos/{id}");
      return response.Result ?? new Todo();
    }
  }
}
