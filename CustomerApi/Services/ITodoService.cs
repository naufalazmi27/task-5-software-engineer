﻿using CustomerApi.Entities;

namespace CustomerApi.Services
{
  public interface ITodoService
  {
    public Task<List<Todo>> GetAll();
    public Todo GetById(int id);
  }
}
