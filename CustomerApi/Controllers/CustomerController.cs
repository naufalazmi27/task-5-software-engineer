﻿using CustomerApi.Entities;
using CustomerApi.Models.Requests;
using CustomerApi.Services;
using CustomerApi.Utils;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;

namespace CustomerApi.Controllers
{
  [Route("api/customers")]
  [ApiController]
  public class CustomerController : ControllerBase
  {

    private readonly ICustomerService _customerService;
    private readonly ILogger<CustomerController> _logger;

    public CustomerController(ICustomerService customerService, ILogger<CustomerController> logger)
    {
      _customerService = customerService;
      _logger = logger;
    }


    // POST api/<CustomerController>
    [HttpPost]
    public IActionResult Post([FromBody] Customer customer)
    {
      try
      {
        _logger.LogInformation($"Started Create Customer API, payload: {JsonSerializer.Serialize(customer)}");
        _customerService.Add(customer);

        return Helper.generateSuccessResponse<string>(message: "created", statusCode: 201);
      }
      catch (BadHttpRequestException httpException)
      {
        _logger.LogError(httpException.Message, httpException);
        return Helper.generateBadResponse(httpException.Message, (int)httpException.StatusCode);
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return Helper.generateBadResponse(ex.Message);
      }
    }

    // GET: api/<ValuesController>
    [HttpGet]
    public IActionResult Get(
      [FromQuery] GetCustomerRequestDto request
      )
    {
      try
      {
        _logger.LogInformation($"Started Get Customer API, payload: {JsonSerializer.Serialize(request)}");
        var customers = _customerService.GetAll(request.offset, request.limit ?? 25);

        return Helper.generateSuccessResponse(result: customers);
      }
      catch (BadHttpRequestException httpException)
      {
        _logger.LogError(httpException.Message, httpException);
        return Helper.generateBadResponse(httpException.Message, (int)httpException.StatusCode);
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return Helper.generateBadResponse(ex.Message);
      }
    }

    // GET api/<ValuesController>/5
    [HttpGet("{id}")]
    public IActionResult Get(int id)
    {
      try
      {
        _logger.LogInformation($"Started Get Customer By Id API, Id: {id}");
        var customer = _customerService.SearchById(id);

        return Helper.generateSuccessResponse(result: customer);
      }
      catch (BadHttpRequestException httpException)
      {
        _logger.LogError(httpException.Message, httpException);
        return Helper.generateBadResponse(httpException.Message, (int)httpException.StatusCode);
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return Helper.generateBadResponse(ex.Message);
      }
    }

    // PUT api/<CustomerController>
    [HttpPut]
    public IActionResult Put([FromBody] Customer customer)
    {
      try
      {
        _logger.LogInformation($"Started Update Customer API, payload: {JsonSerializer.Serialize(customer)}");
        _customerService.Update(customer);

        return Helper.generateSuccessResponse<string>(message: "updated", statusCode: 200);
      }
      catch (BadHttpRequestException httpException)
      {
        _logger.LogError(httpException.Message, httpException);
        return Helper.generateBadResponse(httpException.Message, (int)httpException.StatusCode);
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return Helper.generateBadResponse(ex.Message);
      }
    }

    // DELETE api/<CustomerController>
    [HttpDelete("{id}")]
    public IActionResult Delete(int id)
    {
      try
      {
        _logger.LogInformation($"Started Delete Customer API, id: {id}");
        _customerService.Remove(id);

        return Helper.generateSuccessResponse<string>(message: "removed", statusCode: 200);
      }
      catch (BadHttpRequestException httpException)
      {
        _logger.LogError(httpException.Message, httpException);
        return Helper.generateBadResponse(httpException.Message, (int)httpException.StatusCode);
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return Helper.generateBadResponse(ex.Message);
      }
    }

  }
}