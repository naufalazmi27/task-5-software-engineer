﻿using CustomerApi.Entities;
using CustomerApi.Services;
using CustomerApi.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;

namespace CustomerApi.Controllers
{
  [Route("api/todos")]
  [ApiController]
  public class TodoController : ControllerBase
  {

    private readonly ITodoService _todoService;
    private readonly ILogger<TodoController> _logger;

    public TodoController(ITodoService todoService, ILogger<TodoController> logger)
    {
      _todoService = todoService;
      _logger = logger;
    }

    [HttpGet]
    public async Task<IActionResult> Get()
    {
      try
      {
        _logger.LogInformation($"Started Get Todos API");
        var todos = await _todoService.GetAll();

        return Helper.generateSuccessResponse(result: todos);
      }
      catch (BadHttpRequestException httpException)
      {
        _logger.LogError(httpException.Message, httpException);
        return Helper.generateBadResponse(httpException.Message, (int)httpException.StatusCode);
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return Helper.generateBadResponse(ex.Message);
      }
    }

    [HttpGet("{id}")]
    public IActionResult Get(int id)
    {
      try
      {
        _logger.LogInformation($"Started Get Todos API By Id, Id: {id}");
        var todo = _todoService.GetById(id);

        return Helper.generateSuccessResponse(result: todo);
      }
      catch (BadHttpRequestException httpException)
      {
        _logger.LogError(httpException.Message, httpException);
        return Helper.generateBadResponse(httpException.Message, (int)httpException.StatusCode);
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return Helper.generateBadResponse(ex.Message);
      }
    }
  }
}
