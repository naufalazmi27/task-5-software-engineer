﻿using CustomerApi.Entities;

namespace CustomerApi.Repositories
{
  public class CustomerRepository : ICustomerRepository
  {
    private readonly CustomerDbContext _context;
    public CustomerRepository(CustomerDbContext context)
    {
      _context = context;
    }

    void ICustomerRepository.Add(Customer customer)
    {
      _context.Customers.Add(customer);
      _context.SaveChanges();
    }

    IEnumerable<Customer> ICustomerRepository.GetAll(int offset, int limit)
    {
      var customers = _context.Customers
        .OrderBy(customer => customer.Id)
        .Skip(offset)
        .Take(limit)
        .ToList();

      return customers;
    }

    void ICustomerRepository.Remove(int customerId)
    {
      var customer = _context.Customers.First(customer => customer.Id == customerId);
      _context.Customers.Remove(customer);
      _context.SaveChanges();
    }

    Customer? ICustomerRepository.SearchById(int customerId)
    {
      var customer = _context.Customers
        .Find(customerId);

      return customer;
    }

    void ICustomerRepository.Update(Customer customer)
    {
      _context.Customers.Update(customer);
      _context.SaveChanges();
    }
  }
}
