﻿using CustomerApi.Entities;

namespace CustomerApi.Repositories
{
    public interface ICustomerRepository
    {
        void Add(Customer customer);
        IEnumerable<Customer> GetAll(int offset, int limit);
        Customer? SearchById(int customerId);
        void Update(Customer customer);
        void Remove(int customerId);
    }
}
