﻿namespace CustomerApi.Models.Requests
{
  public class GetCustomerRequestDto
  {
    public int offset { get; set; }
    public int? limit { get; set; }
  }
}
