﻿namespace CustomerApi.Models.Responses
{
  public class SuccessResponse<T>
  {
    public T? result {  get; set; }
    public string status { get; set; } = "success";
  }
}
