﻿namespace CustomerApi.Models.Responses
{
  public class BadResponse
  {
    public int errorCode {  get; set; }
    public string errorMessage { get; set; }
  }
}
