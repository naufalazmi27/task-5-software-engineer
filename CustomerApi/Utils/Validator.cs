﻿using System.ComponentModel.DataAnnotations;

namespace CustomerApi.Utils
{
  public class Validator
  {
    public static bool isValidEmail(string email)
    {
      return new EmailAddressAttribute().IsValid(email);
    }
  }
}
