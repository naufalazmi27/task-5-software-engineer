﻿using CustomerApi.Models.Responses;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace CustomerApi.Utils
{
  public class Helper
  {
    public static IActionResult generateBadResponse(string errorMessage, int errorCode = 500)
    {
      var response = new BadResponse() { errorCode = errorCode, errorMessage = errorMessage };
      return new JsonResult(response)
      {
        ContentType = "application/json",
        StatusCode = errorCode
      };
    }

    public static IActionResult generateSuccessResponse<T>(T? result = default(T), string? message = "success", int? statusCode = 200)
    {
      var response = new SuccessResponse<T?>() { result = result, status = message};
      return new JsonResult(response)
      {
        ContentType = "application/json",
        StatusCode = statusCode
      };
    }
  }
}
