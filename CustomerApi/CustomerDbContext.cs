﻿using CustomerApi.Entities;
using Microsoft.EntityFrameworkCore;

namespace CustomerApi
{
    public class CustomerDbContext : DbContext
    {
        public CustomerDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Customer> Customers { get; set; }
    }
}
